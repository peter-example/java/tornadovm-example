package hk.quantr.tornadovm.example;

import java.math.BigDecimal;
import uk.ac.manchester.tornado.api.ImmutableTaskGraph;
import uk.ac.manchester.tornado.api.TaskGraph;
import uk.ac.manchester.tornado.api.TornadoExecutionPlan;
import uk.ac.manchester.tornado.api.annotations.Parallel;
import uk.ac.manchester.tornado.api.common.TornadoDevice;
import uk.ac.manchester.tornado.api.enums.DataTransferMode;
import uk.ac.manchester.tornado.api.runtime.TornadoRuntime;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class Init {

	private static final boolean CHECK = true;

	public static void compute(float[] array) {
		for (@Parallel int i = 0; i < array.length; i++) {
			array[i] = array[i] + 100;
		}
	}

	public static void main(String[] args) {

		int size = 300000000;
		BigDecimal bytesToAllocate = new BigDecimal(((float) ((long) (size) * 4) * (float) 1E-6));
		System.out.println("Running with size: " + size);
		System.out.println("Input size: " + bytesToAllocate + " (MB)");
		float[] array = new float[size];

		try {
			Class.forName("uk.ac.manchester.tornado.api.runtime.TornadoRuntime");
		} catch (ClassNotFoundException ex) {
			System.err.println("Tornado not loaded");
			return;
		}
		TornadoDevice device = TornadoRuntime.getTornadoRuntime().getDriver(0).getDevice(0);
		long maxDeviceMemory = device.getMaxAllocMemory();
		double mb = maxDeviceMemory * 1E-6;
		System.out.println("Maximum alloc device memory: " + mb + " (MB)");

		TaskGraph taskGraph = new TaskGraph("s0") //
				.task("t0", Init::compute, array) //
				.transferToHost(DataTransferMode.EVERY_EXECUTION, array);

		ImmutableTaskGraph immutableTaskGraph = taskGraph.snapshot();
		TornadoExecutionPlan executor = new TornadoExecutionPlan(immutableTaskGraph);
		executor.execute();

		if (CHECK) {
			boolean check = true;
			for (float v : array) {
				if (v != 100) {
					check = false;
					break;
				}
			}
			if (!check) {
				System.out.println("Result is wrong");
			} else {
				System.out.println("Result is correct");
			}
		}
	}
}
