package hk.quantr.tornadovm.example;

import java.util.Random;
import java.util.stream.IntStream;

import uk.ac.manchester.tornado.api.ImmutableTaskGraph;
import uk.ac.manchester.tornado.api.TaskGraph;
import uk.ac.manchester.tornado.api.TornadoExecutionPlan;
import uk.ac.manchester.tornado.api.annotations.Parallel;
import uk.ac.manchester.tornado.api.enums.DataTransferMode;

public class MultipleTasks {

	private static final int MAX_ITERATIONS = 100;

	private static void foo(float[] x, float[] y) {
		for (@Parallel int i = 0; i < y.length; i++) {
			y[i] = x[i] + 100;
		}
	}

	private static void bar(float[] y) {
		for (@Parallel int i = 0; i < y.length; i++) {
			y[i] = y[i] + 200;
		}
	}

	public static void main(String[] args) {

		int numElements = 512;

		if (args.length > 0) {
			numElements = Integer.parseInt(args[0]);
		}

		final float[] x = new float[numElements];
		final float[] y = new float[numElements];

		Random r = new Random();
		IntStream.range(0, numElements).parallel().forEach(i -> x[i] = r.nextFloat());

		TaskGraph taskGraph = new TaskGraph("example") //
				.transferToDevice(DataTransferMode.EVERY_EXECUTION, x) //
				.task("foo", MultipleTasks::foo, x, y) //
				.task("bar", MultipleTasks::bar, y) //
				.transferToHost(DataTransferMode.EVERY_EXECUTION, y);

		ImmutableTaskGraph immutableTaskGraph = taskGraph.snapshot();
		TornadoExecutionPlan executor = new TornadoExecutionPlan(immutableTaskGraph);
		executor.execute();

		for (int i = 0; i < MAX_ITERATIONS; i++) {
			executor.execute();
		}
		System.out.println("end");
	}
}
