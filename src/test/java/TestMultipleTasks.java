
import java.util.Random;
import java.util.stream.IntStream;
import org.junit.Test;
import uk.ac.manchester.tornado.api.ImmutableTaskGraph;
import uk.ac.manchester.tornado.api.TaskGraph;
import uk.ac.manchester.tornado.api.TornadoExecutionPlan;
import uk.ac.manchester.tornado.api.annotations.Parallel;
import uk.ac.manchester.tornado.api.enums.DataTransferMode;

/**
 *
 * @author Peter
 */
public class TestMultipleTasks {

	private static final int MAX_ITERATIONS = 100;

	private static void foo(float[] x, float[] y) {
		for (@Parallel int i = 0; i < y.length; i++) {
			y[i] = x[i] + 100;
		}
	}

	private static void bar(float[] y) {
		for (@Parallel int i = 0; i < y.length; i++) {
			y[i] = y[i] + 200;
		}
	}

	@Test
	public void test() {
		int numElements = 512;
		final float[] x = new float[numElements];
		final float[] y = new float[numElements];

		Random r = new Random();
		IntStream.range(0, numElements).parallel().forEach(i -> x[i] = r.nextFloat());

		TaskGraph taskGraph = new TaskGraph("example")
				.transferToDevice(DataTransferMode.EVERY_EXECUTION, x)
				.task("foo", TestMultipleTasks::foo, x, y)
				.task("bar", TestMultipleTasks::bar, y)
				.transferToHost(DataTransferMode.EVERY_EXECUTION, y);

		ImmutableTaskGraph immutableTaskGraph = taskGraph.snapshot();
		TornadoExecutionPlan executor = new TornadoExecutionPlan(immutableTaskGraph);
		executor.execute();

		for (int i = 0; i < MAX_ITERATIONS; i++) {
			executor.execute();
		}
	}
}
